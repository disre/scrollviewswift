//
//  ViewController.swift
//  ScrollViewSwift
//
//  Created by Kelson Vella on 9/6/17.
//  Copyright © 2017 Kelson Vella. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let scrollview = UIScrollView(frame: self.view.bounds)
        scrollview.backgroundColor = UIColor.blue
        self.view.addSubview(scrollview)
        scrollview.isPagingEnabled = true
        
        scrollview.contentSize = CGSize(width: self.view.bounds.size.width * 3, height: self.view.bounds.size.height * 3)
        
        let view = UIView(frame: CGRect(x: self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view.backgroundColor = UIColor.red
        
        scrollview.addSubview(view)
        
        let view1 = UIView(frame: CGRect(x: self.view.bounds.size.width*2, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view1.backgroundColor = UIColor.brown
        
        scrollview.addSubview(view1)
        
        let view2 = UIView(frame: CGRect(x: 0, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view2.backgroundColor = UIColor.cyan
        
        scrollview.addSubview(view2)
        
        let view3 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view3.backgroundColor = UIColor.magenta
        
        scrollview.addSubview(view3)
        
        let view4 = UIView(frame: CGRect(x: self.view.bounds.size.width*2, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view4.backgroundColor = UIColor.gray
        
        scrollview.addSubview(view4)
        
        let view5 = UIView(frame: CGRect(x: 0, y: self.view.bounds.size.height*2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view5.backgroundColor = UIColor.purple
        
        scrollview.addSubview(view5)
        
        let view6 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height*2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view6.backgroundColor = UIColor.yellow
        
        scrollview.addSubview(view6)
        
        let view7 = UIView(frame: CGRect(x: self.view.bounds.size.width*2, y: self.view.bounds.size.height*2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view7.backgroundColor = UIColor.orange
        
        scrollview.addSubview(view7)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

